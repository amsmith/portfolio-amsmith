const express = require('express')
const compression = require('compression')
const path = require('path')
const next = require('next')
const mongoose = require('mongoose')
const bodyParser = require('body-parser')
const cors = require('cors')
const routes = require('../routes')

// SERVICE
const authService = require('./services/auth')

const dev = process.env.NODE_ENV !== 'production'
const app = next({ dev })
const handle = routes.getRequestHandler(app)
const config = require('./config')

const bookRoutes = require('./routes/book')
const projectRoutes = require('./routes/project')

const robotsOptions = {
  root: path.join(__dirname, '../static'),
  headers: {
    'Content-Type': 'text/plain;charset=UTF-8'
  }
}

const secretData = [
  {
    title: 'SecretData1',
    description: 'Plans how to build spaceship'
  },
  {
    title: 'SecretData2',
    description: 'My secret passwords'
  }
]

// Connect to database
mongoose
  .connect(config.DB_URI, { useNewUrlParser: true })
  .then(() => {})
  .catch(err => console.log(err))

// Create basic custom Node web server for Next.js clean URLs
app
  .prepare()
  .then(() => {
    const server = express()
    server.use(compression())

    // Add middleware for body-parser
    server.use(bodyParser.json())

    // Allow all CORS requests
    server.use(cors())

    // Use routes from routes/project
    server.use('/api/v1/books', bookRoutes)
    server.use('/api/v1/projects', projectRoutes)

    server.get('/robots.txt', (req, res) => {
      return res.status(200).sendFile('robots.txt', robotsOptions)
    })

    // Use middleware to ensure user is authenticated
    server.get('/api/v1/secret', authService.checkJWT, (req, res) => {
      return res.json(secretData)
    })

    // Use middleware to ensure user has permission
    server.get(
      '/api/v1/onlysiteowner',
      authService.checkJWT,
      authService.checkRole('siteOwner'),
      (req, res) => {
        return res.json(secretData)
      }
    )

    server.get('*', (req, res) => {
      return handle(req, res)
    })

    // Generate error when user does not have valid token
    server.use(function(err, req, res, next) {
      if (err.name === 'UnauthorizedError') {
        res.status(401).send({ title: 'Unauthorized', detail: 'Invalid token' })
      }
    })

    const PORT = process.env.PORT || 3000

    server.use(handle).listen(PORT, err => {
      if (err) throw err
      console.log('> Ready on port ' + PORT)
    })
  })
  .catch(ex => {
    console.error(ex.stack)
    process.exit(1)
  })
