const mongoose = require('mongoose')
const Schema = mongoose.Schema

const projectSchema = new Schema({
  userId: { type: String, required: true },
  title: { type: String, required: true, maxlength: 256 },
  type: { type: String, required: true, maxlength: 256 },
  tech: { type: String, required: true, maxlength: 256 },
  repo: { type: String, required: true, maxlength: 256 },
  liveApp: { type: String, required: true, maxlength: 256 },
  description: { type: String, required: true, maxlength: 2048 },
  startDate: { type: Date, required: true },
  endDate: Date
})

module.exports = mongoose.model('Project', projectSchema)
