const jwt = require('express-jwt')
const jwksRsa = require('jwks-rsa')

const config = require('../config')
const NAMESPACE = config.NAMESPACE

// MIDDLEWARE
// Check for valid token
exports.checkJWT = jwt({
  secret: jwksRsa.expressJwtSecret({
    cache: true,
    rateLimit: true,
    jwksRequestsPerMinute: 50,
    jwksUri: 'https://dev-5vugtflr.auth0.com/.well-known/jwks.json'
  }),
  audience: '4yYouR5wi0rvYyaTLk55GytnzukSiWIe',
  issuer: 'https://dev-5vugtflr.auth0.com/',
  algorithms: ['RS256']
})

exports.checkRole = role => (req, res, next) => {
  const user = req.user

  if (user && user[NAMESPACE + '/role'] && user[NAMESPACE + '/role'] === role) {
    next()
  } else {
    return res.status(401).sent({
      title: 'Not Authorized',
      detail: 'You do not have permission to view this page.'
    })
  }
}
