const express = require('express')
const router = express.Router()
const projectController = require('../controllers/project')
const authService = require('../services/auth')

// Get a list of all projects
router.get('', projectController.getProjects)

// Get a project by ID
router.get('/:id', projectController.getProjectById)

// Add a new project
router.post(
  '',
  authService.checkJWT,
  authService.checkRole('siteOwner'),
  projectController.saveProject
)

// Update project data
router.patch(
  '/:id',
  authService.checkJWT,
  authService.checkRole('siteOwner'),
  projectController.updateProject
)

// Delete project
router.delete(
  '/:id',
  authService.checkJWT,
  authService.checkRole('siteOwner'),
  projectController.deleteProject
)

module.exports = router
