const express = require('express')
const router = express.Router()
const bookController = require('../controllers/book')

// Add a new book
router.post('', bookController.saveBook)

// Get a list of all books
router.get('', bookController.getBooks)

// Update book data
router.patch('/:id', bookController.updateBook)

// Delete book
router.delete('/:id', bookController.deleteBook)

module.exports = router
