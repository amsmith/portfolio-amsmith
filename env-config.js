const prod = process.env.NODE_ENV === 'production'

module.exports = {
  'process.env.BASE_URL': prod
    ? 'https://asmith.herokuapp.com'
    : 'http://localhost:3000',
  'process.env.NAMESPACE': 'https://asmith.herokuapp.com',
  'process.env.CLIENT_ID': '4yYouR5wi0rvYyaTLk55GytnzukSiWIe'
}
