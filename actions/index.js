import axios from 'axios'
import Cookies from 'js-cookie'

import { getCookieFromReq } from '../helpers/utils'

// Axios config
const axiosInstance = axios.create({
  baseURL: `${process.env.BASE_URL}/api/v1`,
  timeout: 3000
})

// Set token value
const setAuthHeader = req => {
  const token = req ? getCookieFromReq(req, 'jwt') : Cookies.getJSON('jwt')

  if (token) {
    return { headers: { authorization: `Bearer ${token}` } }
  }
  return undefined
}

// General error handling
const rejectPromise = resError => {
  let error = {}
  if (resError && resError.response && resError.response.data) {
    error = resError.response.data
  } else {
    error = resError
  }
  return Promise.reject(error)
}

// Create "secret" route to check ability to protect routes
export const getSecretData = async req => {
  return await axiosInstance
    .get('/secret', setAuthHeader(req))
    .then(response => {
      return response.data
    })
}

// List of all projects
export const getProjects = async () => {
  return await axiosInstance.get('/projects').then(response => {
    return response.data
  })
}

// Get project by ID
export const getProjectById = async id => {
  return await axiosInstance.get(`/projects/${id}`).then(response => {
    return response.data
  })
}

// Create new project
export const createProject = async projectData => {
  return await axiosInstance
    .post('/projects', projectData, setAuthHeader())
    .then(response => response.data)
    .catch(error => rejectPromise(error))
}

// Update project
export const updateProject = async projectData => {
  return await axiosInstance
    .patch(`/projects/${projectData._id}`, projectData, setAuthHeader())
    .then(response => response.data)
    .catch(error => rejectPromise(error))
}

// Delete project
export const deleteProject = projectId => {
  return axiosInstance
    .delete(`/projects/${projectId}`, setAuthHeader())
    .then(response => response.data)
    .catch(error => rejectPromise(error))
}
