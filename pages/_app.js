import React from 'react'
import App, { Container } from 'next/app'
import auth0 from '../services/auth0'

// Import styles
import 'bootstrap/dist/css/bootstrap.min.css'
import '../styles/main.scss'

export default class MyApp extends App {
  static async getInitialProps({ Component, ctx }) {
    let pageProps = {}
    // Check if the current process is occurring in browser
    // If executing in browser, call client authentication
    // If executing on the Node server, execute server auth
    const user = process.browser
      ? await auth0.clientAuth()
      : await auth0.serverAuth(ctx.req)

    if (Component.getInitialProps) {
      pageProps = await Component.getInitialProps(ctx)
    }

    // Check if the logged in user is the owner of the current site
    const isSiteOwner =
      user && user[process.env.NAMESPACE + '/role'] === 'siteOwner'

    // Convert user to a boolean - false if undefined, true if has a value
    const auth = { user, isAuthenticated: !!user, isSiteOwner }

    return { pageProps, auth }
  }

  render() {
    const { Component, pageProps, auth } = this.props

    return (
      <Container>
        {/* Render component received as initial props
        Pass initial props, user info, and auth state as props to component.
      */}
        <Component {...pageProps} auth={auth} />
      </Container>
    )
  }
}
