import React, { Component } from 'react'

import BaseLayout from '../components/layouts/BaseLayout'
import BasePage from '../components/layouts/BasePage'

import withAuth from '../components/hoc/withAuth'

class Owner extends Component {
  render() {
    return (
      <BaseLayout {...this.props.auth}>
        <BasePage>
          <p>Owner Page</p>
        </BasePage>
      </BaseLayout>
    )
  }
}

const withSpecificAuth = withAuth('siteOwner')

export default withSpecificAuth(Owner)
