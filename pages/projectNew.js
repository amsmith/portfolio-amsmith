import React, { Component } from 'react'
import { Row, Col } from 'reactstrap'
import moment from 'moment'

import BaseLayout from '../components/layouts/BaseLayout'
import BasePage from '../components/layouts/BasePage'
import ProjectCreateForm from '../components/projects/ProjectCreateForm'
import { createProject } from '../actions'
import withAuth from '../components/hoc/withAuth'
import { Router } from '../routes'

const INITIAL_VALUES = {
  title: '',
  type: '',
  tech: '',
  repo: '',
  liveApp: '',
  description: '',
  startDate: moment(),
  endDate: moment()
}

class projectNew extends Component {
  constructor(props) {
    super(props)

    this.state = {
      error: undefined
    }

    this.saveProject = this.saveProject.bind(this)
  }

  saveProject(projectData, { setSubmitting }) {
    setSubmitting(true)

    createProject(projectData)
      .then(project => {
        setSubmitting(false)
        this.setState({ error: undefined })
        Router.pushRoute('/projects')
      })
      .catch(err => {
        const error = err.message || 'Server Error'
        setSubmitting(false)
        this.setState({ error })
      })
  }

  render() {
    const { error } = this.state
    return (
      <BaseLayout {...this.props.auth}>
        <BasePage className='project-create-page' title='Create New Project'>
          <Row>
            <Col md='6'>
              <ProjectCreateForm
                initialValues={INITIAL_VALUES}
                error={error}
                onSubmit={this.saveProject}
              />
            </Col>
          </Row>
        </BasePage>
      </BaseLayout>
    )
  }
}

export default withAuth('siteOwner')(projectNew)
