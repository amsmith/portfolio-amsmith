import React, { Component } from 'react'
import { Row, Col } from 'reactstrap'

import BaseLayout from '../components/layouts/BaseLayout'
import BasePage from '../components/layouts/BasePage'

export default class About extends Component {
  render() {
    return (
      <BaseLayout title='Andrew Smith - About Me' {...this.props.auth}>
        <BasePage className='about-page'>
          <Row>
            <Col className='col-md-6'>
              <div className='left-side'>
                <h1 className='title fadein'>Welcome!</h1>
                <h4 className='subtitle fadein'>Learn More About Me</h4>
                <p className='subsubTitle fadein'>
                  Feel free to contact me with any questions.
                </p>
                <a
                  href='mailto:andrew@asmith.us'
                  className='btn btn-email my-3'
                >
                  Email Me
                </a>
              </div>
            </Col>
            <Col md='6'>
              <div className='fadein'>
                <p>
                  I am a Full Stack Web Developer who takes pride in efficiently
                  developing high-quality software that meets stakeholder needs.
                </p>
                <p>
                  I have worked as a software developer since 2017 with a focus
                  on building modern web applications with technologies such as
                  React.js, Node.js, Python, Django, and Amazon Web Services. I
                  also have an MBA from Northwestern University (Kellogg) along
                  with many years of product management, marketing, and business
                  development experience.
                </p>
                <p>
                  Once I got a taste of programming, I was hooked. I am
                  committed to mastering my craft as a developer. On top of my
                  paid work, I dedicate countless hours to expanding my
                  knowledge and improving my skills.
                </p>
                <p>
                  My development skills in combination with my business
                  expertise provide me with a unique ability to bridge the gap
                  between technology and business. I'm able to develop winning
                  strategies, properly capture requirements, efficiently manage
                  projects, write quality code, and deploy web apps that support
                  business objectives.
                </p>
                <p>
                  If you're interested in working with me, please contact me at
                  andrew@asmith.us or visit me on{' '}
                  <a href='https://www.linkedin.com/in/andrew-smith-6361663/'>
                    LinkedIn
                  </a>
                </p>
              </div>
            </Col>
          </Row>
        </BasePage>
      </BaseLayout>
    )
  }
}
