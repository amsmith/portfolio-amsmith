import React, { Component } from 'react'
import { Row, Col } from 'reactstrap'

import BaseLayout from '../components/layouts/BaseLayout'
import BasePage from '../components/layouts/BasePage'
import ProjectCreateForm from '../components/projects/ProjectCreateForm'
import { updateProject, getProjectById } from '../actions'
import withAuth from '../components/hoc/withAuth'
import { Router } from '../routes'

class projectEdit extends Component {
  static async getInitialProps({ query }) {
    let project = {}
    try {
      project = await getProjectById(query.id)
    } catch (err) {
      console.error(err)
    }
    return { project }
  }

  constructor(props) {
    super(props)

    this.state = {
      error: undefined
    }

    this.updateProject = this.updateProject.bind(this)
  }

  updateProject(projectData, { setSubmitting }) {
    setSubmitting(true)
    updateProject(projectData)
      .then(project => {
        setSubmitting(false)
        this.setState({ error: undefined })
        Router.pushRoute('/projects')
      })
      .catch(err => {
        const error = err.message || 'Server Error'
        setSubmitting(false)
        this.setState({ error })
      })
  }

  render() {
    const { error } = this.state
    const { project } = this.props
    return (
      <BaseLayout {...this.props.auth}>
        <BasePage className='project-create-page' title='Update Project'>
          <Row>
            <Col md='6'>
              <ProjectCreateForm
                initialValues={project}
                error={error}
                onSubmit={this.updateProject}
              />
            </Col>
          </Row>
        </BasePage>
      </BaseLayout>
    )
  }
}

export default withAuth('siteOwner')(projectEdit)
