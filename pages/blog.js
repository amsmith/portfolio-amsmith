import React, { Component } from 'react'

import BaseLayout from '../components/layouts/BaseLayout'
import BasePage from '../components/layouts/BasePage'

export default class Blog extends Component {
  render() {
    return (
      <BaseLayout {...this.props.auth}>
        <BasePage>
          <p>Blog Page</p>
        </BasePage>
      </BaseLayout>
    )
  }
}
