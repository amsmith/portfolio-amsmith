import React, { Component } from 'react'
import { getProjects, deleteProject } from '../actions'
import BaseLayout from '../components/layouts/BaseLayout'
import BasePage from '../components/layouts/BasePage'
import ProjectCard from '../components/projects/ProjectCard'
import { Router } from '../routes'
import { Button, Col, Row } from 'reactstrap'

class Projects extends Component {
  static async getInitialProps() {
    let projects = []
    try {
      projects = await getProjects()
    } catch (err) {
      console.log(err)
    }
    return { projects }
  }

  navigateToEdit(projectId, e) {
    e.stopPropagation()
    Router.pushRoute(`/projects/${projectId}/edit`)
  }

  displayDeleteWarning(projectId, e) {
    e.stopPropagation()
    const isConfirmed = confirm('Are you sure you want to delete this project?')

    if (isConfirmed) {
      this.deleteProject(projectId)
    }
  }

  deleteProject(projectId) {
    deleteProject(projectId)
      .then(() => {
        Router.pushRoute('/projects')
      })
      .catch(err => console.error(err))
  }

  renderProjects(projects) {
    const { isAuthenticated, isSiteOwner } = this.props.auth

    return projects.map((project, index) => {
      return (
        <Col key={index} md='4'>
          <ProjectCard project={project}>
            {isAuthenticated && isSiteOwner && (
              <React.Fragment>
                <Button
                  onClick={e => this.navigateToEdit(project._id, e)}
                  size='sm'
                  color='warning'
                >
                  Edit
                </Button>{' '}
                <Button
                  onClick={e => this.displayDeleteWarning(project._id, e)}
                  size='sm'
                  color='danger'
                >
                  Delete
                </Button>
              </React.Fragment>
            )}
          </ProjectCard>
        </Col>
      )
    })
  }

  render() {
    const { projects } = this.props
    const { isAuthenticated, isSiteOwner } = this.props.auth
    return (
      <BaseLayout title='Andrew Smith - Latest Projects' {...this.props.auth}>
        <BasePage className='project-page' title='Projects'>
          {isAuthenticated && isSiteOwner && (
            <Button
              onClick={() => Router.pushRoute('/projects/new')}
              color='success'
              className='create-project-btn'
            >
              Create Project
            </Button>
          )}
          <Row>{this.renderProjects(projects)}</Row>
        </BasePage>
      </BaseLayout>
    )
  }
}

export default Projects
