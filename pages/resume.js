import React, { Component } from 'react'
import { Row, Col } from 'reactstrap'
import BaseLayout from '../components/layouts/BaseLayout'
import BasePage from '../components/layouts/BasePage'

class Resume extends Component {
  render() {
    return (
      <BaseLayout title='Andrew Smith - Resume' {...this.props.auth}>
        <BasePage title='Get my Résumé' className='resume-page'>
          <Row>
            <Col md={{ size: 8, offset: 2 }}>
              <div className='resume-title'>
                <a
                  href='/static/Smith-Andrew-2019.pdf'
                  download='Smith-Andrew-2019.pdf'
                  className='btn btn-success'
                >
                  Download
                </a>
              </div>
              <iframe
                style={{ width: '100%', height: '800px' }}
                src='/static/Smith-Andrew-2019.pdf'
              />
            </Col>
          </Row>
        </BasePage>
      </BaseLayout>
    )
  }
}

export default Resume
