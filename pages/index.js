import React, { Component } from 'react'
import Typed from 'react-typed'

import BaseLayout from '../components/layouts/BaseLayout'
import { Container, Row, Col } from 'reactstrap'

class Index extends Component {
  constructor(props) {
    super(props)

    this.state = {
      isFlipping: false
    }

    this.technologies = [
      'React.js',
      'Django',
      'Node.js',
      'AWS',
      'Python',
      'Vue.js',
      'SQL',
      'MongoDB'
    ]
  }

  componentDidMount() {
    this.animateCard()
  }

  animateCard() {
    this.cardAnimationInterval = setInterval(() => {
      this.setState({
        isFlipping: !this.state.isFlipping
      })
    }, 6000)
  }

  componentWillUnmount() {
    this.cardAnimationInterval && clearInterval(this.cardAnimationInterval)
  }

  render() {
    const { isAuthenticated, user } = this.props.auth
    const { isFlipping } = this.state

    return (
      <BaseLayout
        className={`cover ${isFlipping ? 'cover-1' : 'cover-0'}`}
        {...this.props.auth}
        headerType='index'
        title='Andrew Smith - Portfolio'
      >
        <div className='main-section'>
          <Container>
            <Row>
              <Col md='6'>
                <div className='hero-section'>
                  <div className={`flipper ${isFlipping ? 'isFlipping' : ''}`}>
                    {/* Card Front */}
                    <div className='front'>
                      <div className='hero-section-content'>
                        <h2> Full Stack Web Developer </h2>
                        <div className='hero-section-content-intro'>
                          Review my portfolio and experience
                        </div>
                      </div>
                      <img
                        className='image'
                        src='/static/images/section-1.jpg'
                        alt='Cartoon Programmer working on laptop.'
                      />
                      <div className='shadow-custom'>
                        <div className='shadow-inner'> </div>
                      </div>
                    </div>
                    {/* Card Back */}
                    <div className='back'>
                      <div className='hero-section-content'>
                        <h2> Product Strategy Expert </h2>
                        <div className='hero-section-content-intro'>
                          Web Apps that Get Results
                        </div>
                      </div>
                      <img
                        className='image'
                        src='/static/images/section-2.jpg'
                        alt='Cartoon Programmer working on laptop.'
                      />
                      <div className='shadow-custom shadow-custom-2'>
                        <div className='shadow-inner'> </div>
                      </div>
                    </div>
                  </div>
                </div>
              </Col>
              <Col md='6' className='hero-welcome-wrapper'>
                <div className='hero-welcome-text'>
                  <h1>
                    Welcome!{isAuthenticated && <span> {user.name}!</span>}
                  </h1>
                  <h2>
                    Here you can begin to learn about my skills, experience, and
                    previous projects. Feel free to email me below to start a
                    conversation.
                  </h2>
                </div>
                <Typed
                  loop
                  typeSpeed={60}
                  backSpeed={40}
                  strings={this.technologies}
                  backDelay={800}
                  loopCount={0}
                  showCursor
                  className='self-typed'
                  cursorChar='|'
                />
                <div className='hero-welcome-bio'>
                  <a
                    href='mailto:andrew@asmith.us'
                    className='btn btn-email my-2'
                  >
                    Email Me
                  </a>
                </div>
              </Col>
            </Row>
          </Container>
          <div className='service-link'>
            Vector illustration credit:{' '}
            <a href='https://www.Vecteezy.com/'>Vecteezy</a>
          </div>
        </div>
      </BaseLayout>
    )
  }
}

export default Index
