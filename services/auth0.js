import auth0 from 'auth0-js'
import Cookies from 'js-cookie'
import jwt from 'jsonwebtoken'
import axios from 'axios'

import { getCookieFromReq } from '../helpers/utils'

const CLIENT_ID = process.env.CLIENT_ID

class Auth0 {
  constructor() {
    this.auth0 = new auth0.WebAuth({
      domain: 'dev-5vugtflr.auth0.com',
      clientID: CLIENT_ID,
      redirectUri: `${process.env.BASE_URL}/callback`,
      responseType: 'token id_token',
      scope: 'openid profile'
    })

    this.login = this.login.bind(this)
    this.logout = this.logout.bind(this)
    this.handleAuthentication = this.handleAuthentication.bind(this)
  }

  login() {
    this.auth0.authorize()
  }

  handleAuthentication() {
    return new Promise((resolve, reject) => {
      this.auth0.parseHash((err, authResult) => {
        if (authResult && authResult.accessToken && authResult.idToken) {
          this.setSession(authResult)
          resolve()
        } else if (err) {
          reject(err)
          console.log(err)
          alert(`Error: ${err.error}. Check the console for further details.`)
        }
      })
    })
  }

  setSession(authResult) {
    // Set the time that the access token will expire
    const expiresAt = JSON.stringify(
      authResult.expiresIn * 1000 + new Date().getTime()
    )

    // Save data in cookies
    Cookies.set('jwt', authResult.idToken)
  }

  // Clear data from cookies
  logout() {
    Cookies.remove('jwt')

    this.auth0.logout({
      returnTo: process.env.BASE_URL,
      clientID: CLIENT_ID
    })
  }

  async getJWKS() {
    // Get JSON web keys from auth0
    const res = await axios.get(
      'https://dev-5vugtflr.auth0.com/.well-known/jwks.json'
    )
    const jwks = res.data
    return jwks
  }

  async verifyToken(token) {
    // Check for a token and decode it if it exists
    if (token) {
      const decodedToken = jwt.decode(token, { complete: true })
      // Explicitly return undefined to avoid crashing the app
      if (!decodedToken) {
        return undefined
      }
      // Get the first key from the array
      const jwks = await this.getJWKS()
      const jwk = jwks.keys[0]

      // BUILD CERTIFICATE
      let cert = jwk.x5c[0]
      // Grab 64 characters at a time and put each sequen on a new line
      cert = cert.match(/.{1,64}/g).join('\n')
      cert = `-----BEGIN CERTIFICATE-----\n${cert}\n-----END CERTIFICATE-----\n`
      // If the jw key ID matches the key ID on the decoded token, return verified token
      if (jwk.kid === decodedToken.header.kid) {
        try {
          const verifiedToken = jwt.verify(token, cert)
          const expiresAt = verifiedToken.exp * 1000

          return verifiedToken && new Date().getTime() < expiresAt
            ? verifiedToken
            : undefined
        } catch (err) {
          return undefined
        }
      }
    }
    return undefined
  }

  async clientAuth() {
    const token = Cookies.getJSON('jwt')
    const verifiedToken = await this.verifyToken(token)
    return verifiedToken
  }

  async serverAuth(req) {
    if (req.headers.cookie) {
      const token = getCookieFromReq(req, 'jwt')
      const verifiedToken = await this.verifyToken(token)

      return verifiedToken
    }
    return undefined
  }
}

const auth0Client = new Auth0()

export default auth0Client
