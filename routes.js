const routes = require('next-routes')

module.exports = routes()
  .add('project', '/project/:id')
  .add('projectNew', '/projects/new')
  .add('projectEdit', '/projects/:id/edit')
