import React from 'react'
import Header from '../shared/Header'
import Head from 'next/head'

function BaseLayout(props) {
  // Receives content of each page as children on props
  // Provides Header and authentication state to each page
  const { className, children, isAuthenticated, isSiteOwner } = props
  const headerType = props.headerType || 'default'
  const title = props.title || 'Andrew Smith Project'

  return (
    <React.Fragment>
      <Head>
        <title>{title}</title>
        <meta
          name='description'
          content='My name is Andrew Smith. I am a Full Stack Web
          Developer working with technologies like React.js, Node.js, Python, and AWS. By combining my extensive product and marketing strategy experience with my software development skills, I create web apps that deliver results.'
        />
        <meta
          name='keywords'
          content='asmith software developer, asmith freelancing, asmith consulting, asmith web development, asmith javascript node python react django'
        />
        <meta
          property='og:title'
          content='Andrew Smith - Developer, Product and Marketing Strategy Consultant'
        />
        <meta property='og:locale' content='en_US' />
        <meta property='og:url' content={`process.env.BASE_URL`} />
        <meta property='og:type' content='website' />
        <meta
          property='og:description'
          content='My name is Andrew Smith. I am a Full Stack Web
          Developer working with technologies like React.js, Node.js, Python, and AWS.'
        />
        <meta name='viewport' content='width=device-width, initial-scale=1' />
        <link
          href='https://fonts.googleapis.com/css?family=Montserrat:400,700'
          rel='stylesheet'
        />
        <link rel='icon' type='image/ico' href='/static/favicon.ico' />
      </Head>
      <div className='layout-container'>
        {/* Display header styling based on page type*/}
        <Header
          className={`port-nav-${headerType}`}
          isAuthenticated={isAuthenticated}
          isSiteOwner={isSiteOwner}
        />
        <main className={`cover ${className}`}>
          <div className='wrapper'>{children}</div>
        </main>
      </div>
    </React.Fragment>
  )
}

export default BaseLayout
