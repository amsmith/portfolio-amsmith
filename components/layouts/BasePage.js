import { Container } from 'reactstrap'

function BasePage(props) {
  // Dynamic styles wrapper
  const { children, className, title } = props
  return (
    <div className={`base-page ${className}`}>
      <Container>
        {title && (
          <div className='page-header'>
            <h1 className='page-header-title'>{title} </h1>{' '}
          </div>
        )}
        {children}
      </Container>
    </div>
  )
}

BasePage.defaultProps = {
  className: ''
}

export default BasePage
