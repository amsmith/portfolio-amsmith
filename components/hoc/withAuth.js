import React from 'react'
import BaseLayout from '../layouts/BaseLayout'
import BasePage from '../layouts/BasePage'

// This is a Higher Order Component (HOC)
// It provides authentication functionality as
// a wrapper for page-based components.
export default role => Component =>
  class withAuth extends React.Component {
    // This HOC must have getInitialProps in order
    // for any downstream components to receive them.
    static async getInitialProps(args) {
      const pageProps =
        (await Component.getInitialProps) &&
        (await Component.getInitialProps(args))
      return { ...pageProps }
    }

    renderProtectedPage() {
      const { isAuthenticated, user } = this.props.auth
      const userRole = user && user[`${process.env.NAMESPACE}/role`]
      let isAuthorized = false

      // Check to see if user has appropriate role to view
      if (role) {
        if (userRole && userRole === role) {
          isAuthorized = true
        }
      } else {
        isAuthorized = true
      }
      // Return component if user is authenticated and authorized
      // Otherwise, display an appropriate message.
      if (!isAuthenticated) {
        return (
          <BaseLayout {...this.props.auth}>
            <BasePage>
              <h1>Please log in to access this page.</h1>
            </BasePage>
          </BaseLayout>
        )
      } else if (!isAuthorized) {
        return (
          <BaseLayout {...this.props.auth}>
            <BasePage>
              <h1>You don't have permission to view this page.</h1>
            </BasePage>
          </BaseLayout>
        )
      } else {
        return <Component {...this.props} />
      }
    }

    render() {
      return this.renderProtectedPage()
    }
  }
