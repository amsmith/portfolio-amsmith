import React from 'react'
import moment from 'moment'
import { Formik, Form, Field } from 'formik'
import { Button, Alert } from 'reactstrap'
import ProjectInput from '../form/ProjectInput'
import ProjectDate from '../form/ProjectDate'

// @param { object } values Values from form inputs
const validateInputs = values => {
  let errors = {}

  Object.entries(values).forEach(([key, value]) => {
    if (!values[key] && key !== 'endDate') {
      errors[key] = `'${key}' is required.`
    }
  })
  const startDate = moment(values.startDate)
  const endDate = moment(values.endDate)

  if (startDate && endDate && endDate.isBefore(startDate)) {
    errors.endDate = 'End Date cannot be before Start Date.'
  }

  return errors
}

const ProjectCreateForm = ({ initialValues, onSubmit, error }) => (
  <div>
    <Formik
      initialValues={initialValues}
      validate={validateInputs}
      onSubmit={onSubmit}
    >
      {({ isSubmitting }) => (
        // Passing props to formik, incl ProjectInput and ProjectDate components
        <Form>
          <Field
            type='text'
            name='title'
            label='Title'
            component={ProjectInput}
          />
          <Field
            type='text'
            name='type'
            label='Type'
            component={ProjectInput}
          />
          <Field
            type='text'
            name='tech'
            label='Technology'
            component={ProjectInput}
          />
          <Field
            type='text'
            name='repo'
            label='Repository'
            component={ProjectInput}
          />
          <Field
            type='text'
            name='liveApp'
            label='Live App'
            component={ProjectInput}
          />
          <Field
            type='textarea'
            name='description'
            label='Description'
            component={ProjectInput}
          />

          <Field
            name='startDate'
            label='Start Date'
            initialDate={initialValues.startDate}
            component={ProjectDate}
          />

          <Field
            name='endDate'
            label='End Date'
            initialDate={initialValues.endDate}
            component={ProjectDate}
            canBeDisabled={true}
          />

          {error && <Alert color='danger'>{error}</Alert>}

          <Button
            color='success'
            size='lg'
            type='submit'
            disabled={isSubmitting}
          >
            Submit
          </Button>
        </Form>
      )}
    </Formik>
  </div>
)

export default ProjectCreateForm
