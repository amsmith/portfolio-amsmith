import React, { Component } from 'react'
import { Card, CardHeader, CardBody, CardTitle, CardText } from 'reactstrap'
import ProjectCardDetail from './ProjectCardDetail'

export default class ProjectCard extends Component {
  constructor(props) {
    super(props)
    this.state = {
      isOpen: false
    }

    this.handleToggle = this.handleToggle.bind(this)
  }

  handleToggle() {
    this.setState({
      isOpen: !this.state.isOpen
    })
  }

  render() {
    const { project, children } = this.props
    const { isOpen } = this.state

    return (
      <span onClick={this.handleToggle}>
        <ProjectCardDetail
          toggle={this.handleToggle}
          project={project}
          isOpen={isOpen}
        />
        <Card className='project-card'>
          <CardHeader className='project-card-header'>
            {project.type}
          </CardHeader>
          <CardBody>
            {/* <p className='project-card-city'> {project.location} </p> */}
            <CardTitle className='project-card-title'>
              {project.title}
            </CardTitle>
            <CardText className='project-card-text'>{project.tech}</CardText>
            <div className='readMore'>{children}</div>
          </CardBody>
        </Card>
      </span>
    )
  }
}
