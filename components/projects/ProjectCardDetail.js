import React from 'react'
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap'
import moment from 'moment'

class ProjectCardDetail extends React.Component {
  render() {
    const { isOpen, toggle, project } = this.props
    return (
      <div>
        <Modal isOpen={isOpen} toggle={toggle}>
          <ModalHeader toggle={toggle}>{project.title}</ModalHeader>
          <ModalBody>
            <p>
              <b>Description:</b> {project.description}
            </p>
            <p>
              <b>Technology:</b> {project.tech}
            </p>
            <p>
              <b>Repository:</b>{' '}
              <a
                href={`${project.repo}`}
                rel='noopener noreferrer'
                target='_blank'
              >
                {project.repo}
              </a>
            </p>
            <p>
              <b>Live App: </b>
              <a
                href={`${project.liveApp}`}
                rel='noopener noreferrer'
                target='_blank'
              >
                {project.liveApp}
              </a>
            </p>
            <p>
              {/* <b>Start Date:</b> {moment(project.startDate).format('MMMM YYYY')} */}
            </p>
            <p>
              <b>Completed:</b>{' '}
              {project.endDate
                ? moment(project.endDate).format('MMMM YYYY')
                : 'Still working here.'}
            </p>
          </ModalBody>
          <ModalFooter>
            <Button color='secondary' onClick={toggle}>
              Close
            </Button>
          </ModalFooter>
        </Modal>
      </div>
    )
  }
}

export default ProjectCardDetail
