import React from 'react'
import DatePicker from 'react-datepicker'
import moment from 'moment'
import { Button, FormGroup, Label } from 'reactstrap'

import 'react-datepicker/dist/react-datepicker.css'

export default class ProjectDate extends React.Component {
  constructor(props) {
    super(props)

    const dateValue = props.initialDate ? moment(props.initialDate) : moment()
    const isHidden = props.initialDate ? false : true

    this.state = {
      dateValue,
      isHidden
    }
    this.handleChange = this.handleChange.bind(this)
  }

  setFieldValueAndTouched(date, touched) {
    const { setFieldValue, setFieldTouched } = this.props.form
    const { name } = this.props.field

    setFieldValue(name, date, true)
    setFieldTouched(name, touched, true)
  }

  handleChange(date) {
    this.setState({
      dateValue: date
    })

    this.setFieldValueAndTouched(date, true)
  }

  toggleDate(date) {
    this.setState({
      isHidden: !this.state.isHidden
    })

    this.setFieldValueAndTouched(date, true)
  }

  render() {
    // From ProjectCreateForm
    const {
      canBeDisabled,
      label,
      field,
      form: { touched, errors }
    } = this.props
    const { isHidden, dateValue } = this.state

    return (
      <FormGroup>
        <Label>{label}</Label>
        <div className='input-group'>
          {!isHidden && (
            <DatePicker
              selected={dateValue}
              onChange={this.handleChange}
              peekNextMonth
              showMonthDropdown
              showYearDropdown
              maxDate={moment()}
              dropdownMode='select'
            />
          )}
        </div>
        {canBeDisabled && !isHidden && (
          <Button size='sm' className='mt-1' onClick={() => this.toggleDate()}>
            Disable
          </Button>
        )}
        {canBeDisabled && isHidden && (
          <React.Fragment>
            <span>
              <em>Disabled</em>
            </span>
            <Button
              className='ml-2'
              size='sm'
              onClick={() => this.toggleDate(dateValue)}
            >
              Set End Date
            </Button>
          </React.Fragment>
        )}
        {touched[field.name] && errors[field.name] && (
          <div className='error'>{errors[field.name]}</div>
        )}
      </FormGroup>
    )
  }
}
